#Sisop Modul 3 

KELOMPOK C10
## Anggota Kelompok

| NRP        | Nama                    |
|:----------:|:-----------------------:|
| 5025211131 | Ali Hasyimi Assegaf     |
| 5025211152 | Frederick Hidayat       |

#Soal 1
a. Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.
```c
        char concat_str[100];
        tmpfile = fopen("temp.txt", "w");
        import_file(fp_in, freq, tmpfile);
        fclose(tmpfile);
        x = 5;

        write(fd2[WRITE_END], &freq, sizeof(unsigned int) * 128);
        write(fd[WRITE_END], &x, sizeof(int));
        write(fd3[WRITE_END], q, sizeof(node));
        write(fd4[1], code, sizeof(code));
        write(fd5[WRITE_END], buf, sizeof(buf));
```
Parent process untuk menghitung frekuensi dan mengirim hasil perhitungan ke child process

b. Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.
```c
        read(fd[READ_END], &x, sizeof(int));
        read(fd2[READ_END], &freq, sizeof(unsigned int) * 128);
        read(fd3[READ_END], q, sizeof(node));
        read(fd4[0], code, sizeof(code));
        read(fd5[READ_END], buf, sizeof(buf));
        if (x == 5)
        {
            huffman_codes = fopen("huffman_codes.txt", "w");
            print_code(freq, huffman_codes); 

            bin_file = fopen("bin_file.bin", "w");
            tmpfile = fopen("temp.txt", "r");
            encode(tmpfile, freq, bin_file);

            fclose(tmpfile);
            fclose(fp_in);
            fclose(bin_file);

            for (i = 0; i < 128; i++)
                input_data += freq[i]; 

            status = 20;
            write(fd7[WRITE_END], &input_data, sizeof(int));
            write(fd8[WRITE_END], &output_data, sizeof(int));
            write(fd6[WRITE_END], &status, sizeof(int));
        }
        exit(0);
```
child process melakukan kompresi file setelah menerima hasil frekuensi dari parent

c. Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
```c
        read(fd[READ_END], &x, sizeof(int));
        read(fd2[READ_END], &freq, sizeof(unsigned int) * 128);
        read(fd3[READ_END], q, sizeof(node));
        read(fd4[0], code, sizeof(code));
        read(fd5[READ_END], buf, sizeof(buf));
        // melakukan kompresi file setelah menerima hasil frequensi dari parent process
        if (x == 5)
        {
            huffman_codes = fopen("huffman_codes.txt", "w");
            print_code(freq, huffman_codes); // print the code table

            // Encode and save the encoded file
            bin_file = fopen("bin_file.bin", "w");
            tmpfile = fopen("temp.txt", "r");
            encode(tmpfile, freq, bin_file);

            fclose(tmpfile);
            fclose(fp_in);
            fclose(bin_file);

            for (i = 0; i < 128; i++)
                input_data += freq[i]; // calculate input bytes

            status = 20;
            write(fd7[WRITE_END], &input_data, sizeof(int));
            write(fd8[WRITE_END], &output_data, sizeof(int));
            write(fd6[WRITE_END], &status, sizeof(int));
        }
        exit(0);

```
child process melakukan kompresi dan encoding menggunakan fungsi encode() dan mengirim hasilnya pada parent process untuk melakukan dekompresi

d. Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi.
```c
        if (status == 20)
        {
            FILE *fp;
            fp = fopen("huffman_codes.txt", "r");
            if (fp == NULL)
            {
                printf("File tidak dapat dibuka\n");
                return 1;
            }

            // membaca kode Huffman dari file dan membuat pohon Huffman
            struct Node *root = createNode('\0');
            char code2[100], c;
            while (fscanf(fp, "%c %s\n", &c, code2) != EOF)
            {
                struct Node *curr = root;
                int len = strlen(code2);
                for (int i = 0; i < len; i++)
                {
                    if (code2[i] == '0')
                    {
                        if (curr->left == NULL)
                        {
                            curr->left = createNode('\0');
                        }
                        curr = curr->left;
                    }
                    else
                    {
                        if (curr->right == NULL)
                        {
                            curr->right = createNode('\0');
                        }
                        curr = curr->right;
                    }
                }
                curr->data = c;
            }

            // menutup file huffman_codes.txt
            // fclose(fp);

            // // membuka file yang telah dikompresi
            fp = fopen("bin_file.bin", "rb");
            if (fp == NULL)
            {
                printf("File tidak dapat dibuka\n");
                return 1;
            }

            // // menghitung ukuran file dan membaca isi file
            fseek(fp, 0, SEEK_END);
            int file_size = ftell(fp);
            fseek(fp, 0, SEEK_SET);
            char *compressed_data = (char *)malloc(file_size + 1);
            fread(compressed_data, 1, file_size, fp);
            compressed_data[file_size] = '\0';

            // menutup file yang telah dikompresi
            fclose(fp);

            // melakukan dekompresi menggunakan kode Huffman
            char *decompressed_data = (char *)malloc(file_size + 1);
            decompress(root, compressed_data, decompressed_data);
            printf("\nHasil dekompresi: %s\n", decompressed_data);

            // menulis hasil dekompresi ke file output
            fp = fopen("decompressed_data.txt", "w");
            if (fp == NULL)
            {
                printf("File tidak dapat dibuka\n");
                return 1;
            }
            fwrite(decompressed_data, 1, strlen(decompressed_data), fp);
            fclose(fp);

        }
```
parent process membaca huffman tree dari file terkompresi dan melakukann dekompresi

d. Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.
```c
            // membebaskan memori yang digunakan
            // free(binaryString);
            read(fd7[READ_END], &input_data, sizeof(int));
            read(fd8[READ_END], &output_data, sizeof(int));
            printf("\nInput bytes:\t\t%d\n", input_data);
            output_data = (output_data % 8) ? (output_data / 8) + 1 : (output_data / 8);
            printf("Output bytes:\t\t%d\n", output_data);

            printf("\nCompression ratio:\t%.2f%%\n\n\n", ((double)(input_data - output_data) / input_data) * 100);

            free(decompressed_data);
```
hitung jumlah bit setelah kompresi menggunakan algoritma huffman dan tampilkan pada layar permbandingan jumlahnya

#Soal 2

a. Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

```c

void matMult(){
	for (int i = 0; i < 4; i++)  {
        for (int j = 0; j < 5; j++)  {
            for (int k = 0; k < 2; k++) {  
                hasilMult[i][j] += matA[i][k] * matB[k][j]; 
            }
        }
    }
}

```
```c
 printf("Proses perkalian matriks A dan B\n");
        
		matMult();
		for (int i = 0; i < row; i++) { 
	        for (int j = 0; j < col; j++)  {
	            matriks[i*col+j] = hasilMult[i][j];
	            printf("%d\t", matriks[i*col+j]);
	        }
	        printf("\n");
	    } 

```

fungsi matMult() akan membantu menghasilkan perkalian sedangkan untuk menampilkan hasilnya akan ditunjukkan oleh kode di bawahnya

```c
int matAUpperBound = 5, matBUpperBound = 4;
		for (int i = 0; i < rowA; i++){
			for(int j = 0; j < colA; j++){
				matA[i][j]=(rand() % (matAUpperBound)) + 1;
			}
		}
		
		for (int i = 0; i < rowB; i++){
			for(int j = 0; j < colB; j++){
				matB[i][j]=(rand() % (matBUpperBound)) + 1;
			}
		}

```

Sedangkan kode diatas digunakan untuk merandom angka dalam matriks.

b. Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)

```c
printf("Matriks diakses dari matriks cinta.c\n");
        
		printf("[[");
		for (int i = 0; i < row; i++) { 
	        for (int j = 0; j < col; j++) {
	            if(j!=col-1) printf("%d,", matriks[i*col+j]);
				else printf("%d", matriks[i*col+j]);
	            arrKelola[i*col+j]=matriks[i*col+j];
	        }
	        if(i!=row-1)printf("], [");
			else printf("]]\n");
	    } 
```

Di atas merupakan cara menampilkan matriks dari kalian.c yang dibantu dengan penerapan shared memory. Penerapan ini terlihat pada shmid dan shmat sebagai kunci utama dalam penerapan shared memory, untuk pengirimannya sendiri, pada kalian.c juga sudah dipasang kunci yang sama sehingga membuat matriks dapat dikirimkan.

kalian.c
```c
  key_t key = 1234;
	    
	    int shmid = shmget(key, sizeof(int)*row*col, IPC_CREAT | 0666);
	    matriks = (int *)shmat(shmid, NULL, 0);

```
```c
printf("Proses perkalian matriks A dan B\n");
        
		matMult();
		for (int i = 0; i < row; i++) { 
	        for (int j = 0; j < col; j++)  {
	            matriks[i*col+j] = hasilMult[i][j];
	            printf("%d\t", matriks[i*col+j]);
	        }
	        printf("\n");
	    } 
		
        sleep(5);
        printf("kalian.c berakhir di sini\n");
        shmdt(matriks);
		shmctl(shmid, IPC_RMID, NULL);

```
Seperti terlihat di kalian.c, terdapat kunci yang sama yang nantinya akan mengirim data matriks dan nantinya dari ketika sleep dipanggil, proses selanjutnya akan dapat mengakses memory tersebut yang nantinya akan ditampilkan dan diolah (cinta.c).

c. Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.

```c
void *faktorial(void *args){
	
	s_args *data;
	data = (s_args *) args;

	number = data->num_fact;
	//printf("For num %lld start\n", number);
	for (int i = number-1; i > 0; i--) number *= i;
	//printf("%lld ", number);
	arrRes[data->index]=number;
	//printf("For num %lld finish\n", number);
	return NULL;
}

```

```c
int totalElement=20;
		pthread_t t_id[20];
		for (int i = 0; i < totalElement; i++){

			str_fac[i].index = i;
			str_fac[i].num_fact = arrKelola[i];

			pthread_create(&t_id[i], NULL, &faktorial, (void*) &str_fac[i]);

		}
		
		for (int i = 0; i < totalElement; i++){
			pthread_join(t_id[i], NULL);
		}
		
		for (int i = 0; i < totalElement; i++)printf("%lld ", arrRes[i]);

```
Di atas adalah kode untuk menampilkan hasil faktorisasi yang diinginkan. pthread_create akan memabantu dalam pembuatan thread yang nantinya dalam program ini akan ada 20 thread yang bekerja bersamaan dalam usaha untuk menghitung faktorial dari tiap angka terkait. thread nantinya akan di join ketika sudah menyelesaikan tugasnya dengan menggunakan pthread_join seperti tampak pada kode di atas. Hasil dari fakotrialnya dapat dilihat di lampiran paling bawah bersamaan dengan subsoal d.

d. Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

Kalian.c dan cinta.c tanpa penghitungan performa

![WhatsApp Image 2023-05-06 at 16 26 52](https://github.com/MrHermes/Modul1_Probstat_5025211152/assets/90272678/dd08a8cc-6097-463a-9c45-87e7a775247c)


Kalian.c dan cinta.c dengan penghitungan performa

![WhatsApp Image 2023-05-06 at 16 53 27](https://github.com/MrHermes/Modul1_Probstat_5025211152/assets/90272678/9864953a-de90-47d6-8b18-339ccf46ef38)


Kalian.c dan sisop.c dengan penghitungan performa (kesalahan printf di bagian sisop.c yang menuliskan cinta.c berakhir di sini yang seharusnya adalah sisop.c berakhir di sini)

![WhatsApp Image 2023-05-06 at 16 53 50](https://github.com/MrHermes/Modul1_Probstat_5025211152/assets/90272678/3e276e19-afd1-4d97-bd8a-b2a5865689a7)


Terlihat di sini bahwa performa program yang menggunakan thread lebih lama daripada program tanpa thread yaitu sisop.c, hal ini dapat dikarenakan thread yang banyak yang membuat program harus melakukan thread switching dalam setiap kali mereka mengakses thread terkait. Pada teorinya sendiri, thread dapat menjadi lebih cepat jika penerapan dapat lebih efisien untuk mencapai performa yang diinginkan (lebih baik).


#Soal 3

a. Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.

```c
        printf("Enter a message to send: ");
        fgets(input, 100, stdin);

        word1 = strtok(input, " ");
        word2 = strtok(NULL, "");

        // If word2 is NULL, set it to "null"
        if (word2 == NULL)
        {
            word2 = "null";
        }
```
dapatkan input dan pecah menjadi dua kata. kata pertama perintah dan kedua judul lagu

```c
        int userID = rand() % 9000 + 1000;
        sprintf(msg.userID, "%d", userID);
```
generate user ID

```c
        if (msgsnd(msgid, &msg, sizeof(struct message) - sizeof(long), 0) == -1)
        {
            perror("msgsnd");
            printf("Error code: %d\n", errno);
            exit(1);
        }
```
send msg ke program stream.c
b. User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.
Proses decrypt dilakukan oleh program stream.c tanpa menggunakan koneksi socket

```c
char *rot13(char *str)
{
    char *p = str;
    while (*p != '\0')
    {
        if (*p >= 'a' && *p <= 'm')
        {
            *p += 13;
        }
        else if (*p >= 'n' && *p <= 'z')
        {
            *p -= 13;
        }
        else if (*p >= 'A' && *p <= 'M')
        {
            *p += 13;
        }
        else if (*p >= 'N' && *p <= 'Z')
        {
            *p -= 13;
        }
        p++;
    }
    return str;
}
```
Berikut merupakan salah satu fungsi decrypt apabila metode yang diminta rot13

c. Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt

```c
        else if (strcmp(msg.mtext, "list") == 0)
        {
            FILE *file2 = fopen("playlist.txt", "r");
            char ch;
            if (file2 == NULL)
            {
                printf("Error opening file");
                return 1;
            }

            // Read and print file contents
            while ((ch = fgetc(file2)) != EOF)
            {
                printf("%c", ch);
            }
            fclose(file2);
        }
```
Program untuk membaca playlist.txt

d. User juga dapat mengirimkan perintah PLAY <SONG> dengan ketentuan.
```c
        else if (strcmp(msg.mtext, "play") == 0)
        {
            int count = 0;
            char filename[] = "playlist.txt";
            char *search_string = malloc(strlen(msg.song) + 1); // +1 for the null terminator
            strcpy(search_string, msg.song);
            printf("%s", search_string);
            FILE *fp;
            char line[MAX_SONGS];
            int line_number = 0;
            int found = 0;
            char matches[100][MAX_SONGS];

            fp = fopen(filename, "r");
            if (fp == NULL)
            {
                printf("Unable to open file '%s'", filename);
                return 1;
            }

            while (fgets(line, MAX_SONGS, fp))
            {
                line_number++;

                // Check if the search string is found in the current line
                if (strstr(line, search_string) != NULL)
                {
                    printf("Found '%s' at line %d: %s", search_string, line_number, line);
                    strcpy(matches[count], line);
                    found = 1;
                    count++;
                }
            }

            if (count == 0)
            {
                printf("THERE IS NO SONG CONTAINING '%s'", search_string);
            }
            else if (count == 1)
            {
                printf("USER %s is PLAYING '%s'", msg.userID, matches[0]);
            }
            else if (count > 1)
            {

                printf("THERE ARE '%d' SONG CONTAINING '%s':\n", count, search_string);
                for (int i = 0; i < count; i++)
                {
                    printf("%d. %s", i + 1, matches[i]);
                }
            }
            fclose(fp);
        }
```
Program untuk membaca lagu yang telah diinputkan

e. User juga dapat menambahkan lagu ke dalam playlist dengan syarat
```c
        else if (strcmp(msg.mtext, "add") == 0)
        {
            char filename[] = "playlist.txt";
            char *new_song = malloc(strlen(msg.song) + 1); // +1 for the null terminator
            strcpy(new_song, msg.song);
            char playlist[MAX_SONGS][MAX_LEN];
            FILE *fp;
            int i, j, num_songs = 0;

            // Read the existing songs from the playlist file
            fp = fopen(filename, "r");
            if (fp == NULL)
            {
                printf("Unable to open file '%s'", filename);
                return 1;
            }

            while (fgets(playlist[num_songs], MAX_LEN, fp))
            {
                num_songs++;
            }

            fclose(fp);

            // Remove the newline character from the new song string
            if (new_song[strlen(new_song) - 1] == '\n')
            {
                new_song[strlen(new_song) - 1] = '\0';
            }

            // Add the new song to the playlist array in the correct position
            for (i = 0; i < num_songs; i++)
            {
                if (strcmp(new_song, playlist[i]) < 0)
                {
                    for (j = num_songs; j > i; j--)
                    {
                        strcpy(playlist[j], playlist[j - 1]);
                    }
                    strcpy(playlist[i], new_song);
                    num_songs++;
                    break;
                }
            }

            // If the new song should be added at the end of the playlist, add it there
            if (i == num_songs)
            {
                strcpy(playlist[num_songs], new_song);
                num_songs++;
            }

            fp = fopen(filename, "w");
            if (fp == NULL)
            {
                printf("Unable to open file '%s'", filename);
                return 1;
            }

            for (i = 0; i < num_songs; i++)
            {
                fputs(playlist[i], fp);
            }

            fclose(fp);

            printf("Song '%s' added to playlist.\n", new_song);
        }
```
Program untuk menambahkan lagu kedalam playlist.txt dan mengurutkan playlist.txt kembali

g. Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".
```c
        else
            printf("UNKNOWN COMMAND");
```
output "UNKNOWN COMMAND" apabila perintah tidak diketahui

#Soal 4

a. Download dan unzip file tersebut dalam kode c bernama unzip.c.

```c
    pid_t pid = fork();

    if (pid == 0)
    {
        char *arg[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", "-O", "hehe.zip", NULL};
        execv("/bin/wget", arg);
        exit(0);
    }
    else
    {
        int status;
        wait(&status);
        char *args[] = {"unzip", "hehe.zip", NULL};
        execv("/bin/unzip", args);
    }

```

Dalam kode diatas, digunakan wget dengan bantuan execv untuk melakukan download terhadap hehe.zip. Download zip terkait dilakukan oleh child program dan pada parent dilakukan unzip terhadap file zip terkait.

b. Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.


```c
void categorize(char* src, char* ext, int* cnt, int max) {
    DIR* src_dir;
    struct dirent* src_ent;
    src_dir = opendir(src);

    if (src_dir == NULL) {
        printf("Failed to open directory %s\n", src);
        exit(EXIT_FAILURE);
    }
    else log_accessed(src);

    while ((src_ent = readdir(src_dir)) != NULL) {
        if (
            strcmp(src_ent->d_name, ".") == 0 ||
            strcmp(src_ent->d_name, "..") == 0
        ) continue;

        if (src_ent->d_type == DT_DIR) {
            char new_src[MAX_SIZE];
            snprintf(new_src, MAX_SIZE, "%s/%s", src, src_ent->d_name);
            categorize(new_src, ext, cnt, max);
        }

        else if (src_ent->d_type == DT_REG) {
            char src_ext[10];
            strcpy(src_ext, get_ext(src_ent->d_name));
            if (strcmp(ext, "other") != 0 && strcmp(lower_case(src_ext), ext) != 0) continue;
            
            int dst_cnt = *cnt / 10 + 1;
            char dst[MAX_SIZE];
            
            if (dst_cnt > 1) snprintf(dst, MAX_SIZE, "categorized/%s (%d)", ext, dst_cnt);
            else snprintf(dst, MAX_SIZE, "categorized/%s", ext);
            if (access(dst, F_OK) != 0) {
                if (mkdir(dst, 0777) != 0) {
                    printf("Failed to make directory %s\n", dst);
                    exit(EXIT_FAILURE);
                }
                else log_made(dst);
            }

            char filename[MAX_SIZE];
            snprintf(filename, MAX_SIZE, "%s/%s", src, src_ent->d_name);
            strcat(dst, "/");
            strcat(dst, src_ent->d_name);
            if (rename(filename, dst) != 0) {
                printf("Failed to move %s to %s\n", filename, dst);
                exit(EXIT_FAILURE);
            }
            else log_moved(ext, filename, dst);
            (*cnt)++;
        }
    }

    closedir(src_dir);
}

```


Pada kode di atas, setiap direktori akan di rekursi untuk mendapatkan setiap bagian konten dari folder files yang mana merupakan bagian dari hehe.zip yang telah di unzipped sebelumnya. Kode di bagian while digunakan untuk melakukan perulangan terus menerus dalam membuka dan menelaah lebih jauh tiap-tiap folder dalam folder files dan mengkategorikan tiap file ke dalam tiap folder yang sesuai dimana nanti nya akan terbentuk dan dihitung jumlah count(isi) dari tiap folder yang ada supaya tidak melebihi 10, jika lebih dari 10 maka akan dibentuk folder baru yang namanya diincrement setiap kali folder dengan jenis yang sama terbentuk.


c. Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
extension_a : banyak_file
extension_b : banyak_file
extension_c : banyak_file
other : banyak_file

Pada sub soal ini digunakan fungsi sort text untuk mengurutkan secara ascending menurut banyak jumlahnya

```c
void sort_exts(Ext* exts, int size) {
    Ext temp;
    for (int i = 0; i < size - 1; i++) {
        for (int j = i + 1; j < size; j++) {
            if (exts[i].cnt < exts[j].cnt) continue;
            temp = exts[i];
            exts[i] = exts[j];
            exts[j] = temp;
        }
    }
}

```

Untuk menampilkan hasilnya akan menggunakan kode di bawah ini

```c
sort_exts(ext, size);
    for (int i = 0; i < size; i++) {
        printf("%s : %d\n", ext[i].name, ext[i].cnt);
    }

```

Dari sinilah akan ditampilkan urutan dari setiap extension beserta jumlahnya

![WhatsApp Image 2023-05-13 at 02 41 28](https://github.com/MrHermes/Modul1_Probstat_5025211152/assets/90272678/497ce0e5-dd9b-4ad1-a8ec-42641e6a1571)

berikut hasil extension-extension yang telah diurutkan berdasarkan kriteria soal

d. Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.

```c
void* th_func(void* thread) {
    Arg* arg = (Arg*) thread;
    int cnt = 0;
    categorize(arg->src, arg->ext, &cnt, arg->max);

    //printf("%d", cnt);
    Ext* result = (Ext*) malloc(sizeof(Ext));
    strcpy(result->name, arg->ext);
    result->cnt = cnt;
    //printf("%s", result->name);
    return result;
}

int extract_ext(Arg *thread, Ext *ext_list){
    
    pthread_t tid[MAX_SIZE];

    FILE* ext = fopen("extensions.txt", "r");

    char ext_type[10];
    int ext_size = 0;

    FILE* max = fopen("max.txt", "r");

    int max_size;
    fscanf(max, "%d", &max_size);

    while (!(fscanf(ext, "%s", ext_type) == EOF)) {
        strcpy(thread[ext_size].src, "files");
        strcpy(thread[ext_size].ext, ext_type);
        thread[ext_size].max = max_size;
        pthread_create(&tid[ext_size], NULL, th_func, &thread[ext_size]);
        ext_size++;
    }

    for (int i = 0; i < ext_size; i++) {
        Ext* result;
        pthread_join(tid[i], (void **) &result);
        ext_list[i] = *result;
    }

    int cnt = 0;
    categorize("files", "other", &cnt, max_size);
    sprintf(ext_list[ext_size].name, "other");
    ext_list[ext_size].cnt = cnt;
    ext_size++;

    // for (int i = 0; i < ext_size; i++) {
    //     printf("%s : %d\n", ext_list[i].name, ext_list[i].cnt);
    // }

    return ext_size;
}

```

Berikut thread dan thread function yang menyertai, dimana digunakan untuk melakukan kategorisasi. Pembuatan thread akan mereturn value seperti yang terlihat pada thread join. Thread akan menunggu value dari variabel return yang mana nantinya akan di assign ke dalam struct ext yang dikehendaki

e. Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
Path dimulai dari folder files atau categorized
Simpan di dalam log.txt
ACCESSED merupakan folder files beserta dalamnya
Urutan log tidak harus sama

```c
FILE* open_log() {
    FILE* log = fopen("log.txt", "a");
    time_t current_time = time(NULL);
    struct tm* local_time;

    current_time = time(NULL);
    local_time = localtime(&current_time);

    fprintf(log, "%02d-%02d-%04d %02d:%02d:%02d ",
        local_time->tm_mday, local_time->tm_mon + 1, local_time->tm_year + 1900,
        local_time->tm_hour, local_time->tm_min, local_time->tm_sec
    );

    return log;
}

void log_accessed(char* path) {
    FILE* log = open_log();
    fprintf(log, "ACCESSED %s\n", path);
    fclose(log);
}

void log_made(char* path) {
    FILE* log = open_log();
    fprintf(log, "MADE %s\n", path);
    fclose(log);
}

void log_moved(char* ext, char* src, char* dst) {
    FILE* log = open_log();
    fprintf(log, "MOVED %s file : %s > %s\n", ext, src, dst);
    fclose(log);
}
```
```c
void clear_log() {
    if (access("log.txt", F_OK) != 0) return;
    remove("log.txt");
}
```

Berikut adalah kode untuk melakukan penulisan log, untuk kode dibagian bawah digunakan untuk menghapus log.txt tiap kali program dijalankan supaya tidak bertumpuk. Kata yang digunakan akan berdasar pada apa perintah yang diberikan menurut fungsi-fungsi di atas, seperti MADE ACCESSED dan MOVED. 

f. Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
Untuk menghitung banyaknya ACCESSED yang dilakukan.
Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.

Bagian atas kode terdapat command system untuk melakukan pengambilan jumlah accesssed yang dilakukan berdasar pada log.txt

```c
system("echo ACCESSED Done: $(cat log.txt | grep ACCESSED | wc -l)");

    FILE* fp = popen("awk -F 'MADE ' '/MADE/ {print $2}' log.txt", "r");
    FILE* ext = fopen("extensions.txt", "r");
    char buf[BUFFER_SIZE];

    printf("\nFolder Created : Total Files\n\n");

    int folders_size = 0;
    DirInfo folders[BUFFER_SIZE];
    while(fscanf(fp, "%[^\n]%*c", buf) != EOF) {
        if (strcmp(buf, "") == 0) break;
        char command[2*BUFFER_SIZE+100];
        sprintf(command, "cat log.txt | grep MOVED | grep '%s' | wc -l", buf);

        int cnt;
        FILE* cnt_fp = popen(command, "r");
        fscanf(cnt_fp, "%d", &cnt);
        //printf("%s : %d", buf, cnt);
        strcpy(folders[folders_size].name, buf);
        folders[folders_size].cnt = cnt;
        folders_size++;
    }

// Bagian extension

    int ext_size = 0;
    DirInfo ext_list[BUFFER_SIZE];
    while (fscanf(ext, "%s", buf) != EOF) {
        char command[2*BUFFER_SIZE+100];
        sprintf(command, "cat log.txt | grep MOVED | grep -i '\\.%s$' | wc -l", buf);

        int cnt;
        FILE* cnt_fp = popen(command, "r");
        fscanf(cnt_fp, "%d", &cnt);

        strcpy(ext_list[ext_size].name, buf);
        ext_list[ext_size].cnt = cnt;
        ext_size++;
    }

```
Berikut kode pengaksesan dari tiap file yang dipindahkan menurut log.txt yang telah dibuat. Tiap datanya di masukan kedalam struct folders yang nantinya akan diurutkan. Begitu juga dengan extension yang akan didata serta dihitung di struct ext_list. 

Kode di bawah ini akan dimanfaatkan untuk mengurutkan folders dan ext_list 
```c
DirInfo temp;
    for (int i = 0; i < folders_size - 1; i++) {
        for (int j = i + 1; j < folders_size; j++) {
            if (folders[i].cnt < folders[j].cnt) continue;
            temp = folders[i];
            folders[i] = folders[j];
            folders[j] = temp;
        }
    }

    for (int i = 0; i < folders_size; i++) {
        if (folders[i].cnt <= 10)
            printf("%s : %d\n", folders[i].name, folders[i].cnt);
    }

for (int i = 0; i < ext_size - 1; i++) {
        for (int j = i + 1; j < ext_size; j++) {
            if (ext_list[i].cnt < ext_list[j].cnt) continue;
            temp = ext_list[i];
            ext_list[i] = ext_list[j];
            ext_list[j] = temp;
        }
    }

    for (int i = 0; i < ext_size; i++) {
        printf("%s : %d\n", ext_list[i].name, ext_list[i].cnt);
    }

```
Setelah mereka diurutkan maka akan ditampilkan sebagai berikut

![WhatsApp Image 2023-05-13 at 02 40 58](https://github.com/MrHermes/Modul1_Probstat_5025211152/assets/90272678/486d0846-a2a9-4868-ac63-7f465f062a86)















