#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <jansson.h>

#define MSG_SIZE 100
#define MAX_SONGS 1000
#define MAX_LEN 256

struct message
{
    long mtype;
    char mtext[MSG_SIZE];
    char song[1000];
    char userID[4];
};

char *rot13(char *str)
{
    char *p = str;
    while (*p != '\0')
    {
        if (*p >= 'a' && *p <= 'm')
        {
            *p += 13;
        }
        else if (*p >= 'n' && *p <= 'z')
        {
            *p -= 13;
        }
        else if (*p >= 'A' && *p <= 'M')
        {
            *p += 13;
        }
        else if (*p >= 'N' && *p <= 'Z')
        {
            *p -= 13;
        }
        p++;
    }
    return str;
}

static const char b64_table[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static int b64_lookup(char c)
{
    if (c >= 'A' && c <= 'Z')
    {
        return c - 'A';
    }
    else if (c >= 'a' && c <= 'z')
    {
        return c - 'a' + 26;
    }
    else if (c >= '0' && c <= '9')
    {
        return c - '0' + 52;
    }
    else if (c == '+')
    {
        return 62;
    }
    else if (c == '/')
    {
        return 63;
    }
    else
    {
        return -1;
    }
}

char *base64_decode(const char *input)
{
    int len = strlen(input);
    if (len % 4 != 0)
    {
        return NULL; // Invalid input length
    }
    int out_len = len / 4 * 3;
    if (input[len - 1] == '=')
    {
        out_len--;
    }
    if (input[len - 2] == '=')
    {
        out_len--;
    }
    char *output = malloc(out_len + 1);
    int i, j;
    for (i = 0, j = 0; i < len; i += 4, j += 3)
    {
        int b0 = b64_lookup(input[i]);
        int b1 = b64_lookup(input[i + 1]);
        int b2 = b64_lookup(input[i + 2]);
        int b3 = b64_lookup(input[i + 3]);
        if (b0 == -1 || b1 == -1)
        {
            free(output);
            return NULL; // Invalid input character
        }
        output[j] = (b0 << 2) | (b1 >> 4);
        if (input[i + 2] != '=')
        {
            if (b2 == -1)
            {
                free(output);
                return NULL; // Invalid input character
            }
            output[j + 1] = (b1 << 4) | (b2 >> 2);
        }
        if (input[i + 3] != '=')
        {
            if (b3 == -1)
            {
                free(output);
                return NULL; // Invalid input character
            }
            output[j + 2] = (b2 << 6) | b3;
        }
    }
    output[out_len] = '\0';
    return output;
}

char *hex_decode(const char *input)
{
    int len = strlen(input);
    if (len % 2 != 0)
    {
        return NULL; // invalid hex string
    }
    char *output = malloc(len / 2 + 1);
    int i;
    for (i = 0; i < len; i += 2)
    {
        sscanf(&input[i], "%2hhx", &output[i / 2]);
    }
    output[len / 2] = '\0';
    return output;
}

int compare(const void *a, const void *b)
{
    const char **ia = (const char **)a;
    const char **ib = (const char **)b;
    return strcmp(*ia, *ib);
}

void sort_playlist()
{
    FILE *fp;
    char *songs[MAX_SONGS];
    char line[MAX_LEN];
    int i, count = 0;

    fp = fopen("playlist.txt", "r");
    if (!fp)
    {
        printf("Failed to open file.\n");
        return;
    }

    while (fgets(line, MAX_LEN, fp))
    {
        songs[count] = strdup(line);
        count++;
    }

    fclose(fp);

    qsort(songs, count, sizeof(char *), compare);

    fp = fopen("playlist.txt", "w");
    if (!fp)
    {
        printf("Failed to open file.\n");
        return;
    }

    for (i = 0; i < count; i++)
    {
        fputs(songs[i], fp);
        free(songs[i]);
    }

    fclose(fp);
}

int main()
{
    key_t key;
    int msgid;
    struct message msg;

    while (1)
    {
        // Generate the same key that was used to create the message queue
        key = ftok("msgq", 'a');

        // Get the message queue using the generated key
        msgid = msgget(key, 0666 | IPC_CREAT);

        if (msgid == -1)
        {
            perror("msgget");
            exit(1);
        }

        // Receive the message with message type 1
        if (msgrcv(msgid, &msg, sizeof(struct message) - sizeof(long), 1, 0) == -1)
        {
            perror("msgrcv");
            exit(1);
        }

        printf("Message received: %s\n", msg.mtext);
        printf("Song received: %s\n", msg.song);

        if (strcmp(msg.mtext, "decrypt") == 0)
        {
            FILE *file = fopen("song-playlist.json", "r");
            if (file == NULL)
            {
                printf("Failed to open file\n");
                exit(1);
            }

            fseek(file, 0, SEEK_END);
            long length = ftell(file);
            fseek(file, 0, SEEK_SET);
            char *buffer = malloc(length);
            fread(buffer, 1, length, file);
            fclose(file);

            json_error_t error;
            json_t *root = json_loads(buffer, 0, &error);
            if (root == NULL)
            {
                printf("Error parsing JSON: %s\n", error.text);
                exit(1);
            }

            FILE *file1 = fopen("playlist.txt", "w");
            size_t index;
            json_t *value;
            json_array_foreach(root, index, value)
            {
                const char *method = json_string_value(json_object_get(value, "method"));
                const char *song = json_string_value(json_object_get(value, "song"));
                if (strcmp(method, "rot13") == 0)
                {
                    char *decoded = rot13(strdup(song));
                    fputs(decoded, file1);
                    fputs("\n", file1);
                    printf("Method: %s, Song: %s\n", method, decoded);
                    free(decoded);
                }
                else if (strcmp(method, "base64") == 0)
                {
                    char *decoded = base64_decode(strdup(song));
                    fputs(decoded, file1);
                    fputs("\n", file1);
                    printf("Method: %s, Song: %s\n", method, decoded);
                    free(decoded);
                }
                else if (strcmp(method, "hex") == 0)
                {
                    char *decoded = hex_decode(strdup(song));
                    fputs(decoded, file1);
                    fputs("\n", file1);
                    printf("Method: %s, Song: %s\n", method, decoded);
                    free(decoded);
                }
                else
                {
                    printf("Unknown method: %s\n", method);
                }
            }
            fclose(file1);
            sort_playlist();

            free(buffer);
            json_decref(root);
        }

        else if (strcmp(msg.mtext, "list") == 0)
        {
            FILE *file2 = fopen("playlist.txt", "r");
            char ch;
            if (file2 == NULL)
            {
                printf("Error opening file");
                return 1;
            }

            // Read and print file contents
            while ((ch = fgetc(file2)) != EOF)
            {
                printf("%c", ch);
            }
            fclose(file2);
        }

        else if (strcmp(msg.mtext, "play") == 0)
        {
            int count = 0;
            char filename[] = "playlist.txt";
            char *search_string = malloc(strlen(msg.song) + 1); // +1 for the null terminator
            strcpy(search_string, msg.song);
            printf("%s", search_string);
            FILE *fp;
            char line[MAX_SONGS];
            int line_number = 0;
            int found = 0;
            char matches[100][MAX_SONGS];

            fp = fopen(filename, "r");
            if (fp == NULL)
            {
                printf("Unable to open file '%s'", filename);
                return 1;
            }

            while (fgets(line, MAX_SONGS, fp))
            {
                line_number++;

                // Check if the search string is found in the current line
                if (strstr(line, search_string) != NULL)
                {
                    printf("Found '%s' at line %d: %s", search_string, line_number, line);
                    strcpy(matches[count], line);
                    found = 1;
                    count++;
                }
            }

            if (count == 0)
            {
                printf("THERE IS NO SONG CONTAINING '%s'", search_string);
            }
            else if (count == 1)
            {
                printf("USER %s is PLAYING '%s'", msg.userID, matches[0]);
            }
            else if (count > 1)
            {

                printf("THERE ARE '%d' SONG CONTAINING '%s':\n", count, search_string);
                for (int i = 0; i < count; i++)
                {
                    printf("%d. %s", i + 1, matches[i]);
                }
            }
            fclose(fp);
        }
        else if (strcmp(msg.mtext, "add") == 0)
        {
            char filename[] = "playlist.txt";
            char *new_song = malloc(strlen(msg.song) + 1); // +1 for the null terminator
            strcpy(new_song, msg.song);
            char playlist[MAX_SONGS][MAX_LEN];
            FILE *fp;
            int i, j, num_songs = 0;

            // Read the existing songs from the playlist file
            fp = fopen(filename, "r");
            if (fp == NULL)
            {
                printf("Unable to open file '%s'", filename);
                return 1;
            }

            while (fgets(playlist[num_songs], MAX_LEN, fp))
            {
                num_songs++;
            }

            fclose(fp);

            // Remove the newline character from the new song string
            if (new_song[strlen(new_song) - 1] == '\n')
            {
                new_song[strlen(new_song) - 1] = '\0';
            }

            // Add the new song to the playlist array in the correct position
            for (i = 0; i < num_songs; i++)
            {
                if (strcmp(new_song, playlist[i]) < 0)
                {
                    for (j = num_songs; j > i; j--)
                    {
                        strcpy(playlist[j], playlist[j - 1]);
                    }
                    strcpy(playlist[i], new_song);
                    num_songs++;
                    break;
                }
            }

            // If the new song should be added at the end of the playlist, add it there
            if (i == num_songs)
            {
                strcpy(playlist[num_songs], new_song);
                num_songs++;
            }

            fp = fopen(filename, "w");
            if (fp == NULL)
            {
                printf("Unable to open file '%s'", filename);
                return 1;
            }

            for (i = 0; i < num_songs; i++)
            {
                fputs(playlist[i], fp);
            }

            fclose(fp);

            printf("Song '%s' added to playlist.\n", new_song);
        }
        else
            printf("UNKNOWN COMMAND");

        // Remove the message queue
        if (msgctl(msgid, IPC_RMID, NULL) == -1)
        {
            perror("msgctl");
            exit(1);
        }
    }
    return 0;
}
