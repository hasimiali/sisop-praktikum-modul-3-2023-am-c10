#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <time.h>

#include <errno.h>

#define MSG_SIZE 100

struct message
{
    long mtype;
    char mtext[MSG_SIZE];
    char song[1000];
    char userID[4];
};

int main()
{
    key_t key;
    int msgid;
    struct message msg;
    srand(time(NULL));

    char input[100];
    char *word1, *word2;

    // Generate a key for the message queue
    key = ftok("msgq", 'a');

    // Create a message queue with the generated key
    msgid = msgget(key, 0666 | IPC_CREAT);

    if (msgid == -1)
    {
        perror("msgget");
        exit(1);
    }

    while (1)
    {
        // Prompt the user to input a message
        printf("Enter a message to send: ");
        fgets(input, 100, stdin);

        word1 = strtok(input, " ");
        word2 = strtok(NULL, "");

        // If word2 is NULL, set it to "null"
        if (word2 == NULL)
        {
            word2 = "null";
        }

        printf("First word: %s\nSecond word: %s\n", word1, word2);
        strcpy(msg.mtext, word1);
        strcpy(msg.song, word2);

        int userID = rand() % 9000 + 1000;
        sprintf(msg.userID, "%d", userID);

        // Remove newline character from the end of the message
        int len = strlen(msg.song);
        if (len > 0 && msg.song[len - 1] == '\n')
        {
            msg.song[len - 1] = '\0';
        }

        len = strlen(msg.mtext);
        if (len > 0 && msg.mtext[len - 1] == '\n')
        {
            msg.mtext[len - 1] = '\0';
        }

        // Prepare the message to be sent
        msg.mtype = 1;

        // Send the message
        if (msgsnd(msgid, &msg, sizeof(struct message) - sizeof(long), 0) == -1)
        {
            perror("msgsnd");
            printf("Error code: %d\n", errno);
            exit(1);
        }

        printf("Message sent: %s\n", msg.mtext);
    }

    return 0;
}
