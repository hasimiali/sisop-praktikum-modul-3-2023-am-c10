#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>
#include <signal.h>

int main()
{

    pid_t pid = fork();

    if (pid == 0)
    {
        char *arg[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", "-O", "hehe.zip", NULL};
        execv("/bin/wget", arg);
        exit(0);
    }
    else
    {
        int status;
        wait(&status);
        char *args[] = {"unzip", "hehe.zip", NULL};
        execv("/bin/unzip", args);
    }
    // char *args[] = {"unzip", "hehe.zip", NULL};
    // execv("bin/unzip", args);
}