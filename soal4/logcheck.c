#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define BUFFER_SIZE 1024

typedef struct
{
    char name[BUFFER_SIZE];
    int cnt;
} DirInfo;

int main()
{
    system("echo ACCESSED Done: $(cat log.txt | grep ACCESSED | wc -l)");

    FILE *fp = popen("awk -F 'MADE ' '/MADE/ {print $2}' log.txt", "r");
    FILE *ext = fopen("extensions.txt", "r");
    char buf[BUFFER_SIZE];

    printf("\nFolder Created : Total Files\n\n");

    int folders_size = 0;
    DirInfo folders[BUFFER_SIZE];
    while (fscanf(fp, "%[^\n]%*c", buf) != EOF)
    {
        if (strcmp(buf, "") == 0)
            break;
        char command[2 * BUFFER_SIZE + 100];
        sprintf(command, "cat log.txt | grep MOVED | grep '%s' | wc -l", buf);

        int cnt;
        FILE *cnt_fp = popen(command, "r");
        fscanf(cnt_fp, "%d", &cnt);
        // printf("%s : %d", buf, cnt);
        strcpy(folders[folders_size].name, buf);
        folders[folders_size].cnt = cnt;
        folders_size++;
    }

    DirInfo temp;
    for (int i = 0; i < folders_size - 1; i++)
    {
        for (int j = i + 1; j < folders_size; j++)
        {
            if (folders[i].cnt < folders[j].cnt)
                continue;
            temp = folders[i];
            folders[i] = folders[j];
            folders[j] = temp;
        }
    }

    for (int i = 0; i < folders_size; i++)
    {
        if (folders[i].cnt <= 10)
            printf("%s : %d\n", folders[i].name, folders[i].cnt);
    }

    printf("\nExtension Types : Total Files\n\n");

    int ext_size = 0;
    DirInfo ext_list[BUFFER_SIZE];
    while (fscanf(ext, "%s", buf) != EOF)
    {
        char command[2 * BUFFER_SIZE + 100];
        sprintf(command, "cat log.txt | grep MOVED | grep -i '\\.%s$' | wc -l", buf);

        int cnt;
        FILE *cnt_fp = popen(command, "r");
        fscanf(cnt_fp, "%d", &cnt);

        strcpy(ext_list[ext_size].name, buf);
        ext_list[ext_size].cnt = cnt;
        ext_size++;
    }

    for (int i = 0; i < ext_size - 1; i++)
    {
        for (int j = i + 1; j < ext_size; j++)
        {
            if (ext_list[i].cnt < ext_list[j].cnt)
                continue;
            temp = ext_list[i];
            ext_list[i] = ext_list[j];
            ext_list[j] = temp;
        }
    }

    for (int i = 0; i < ext_size; i++)
    {
        printf("%s : %d\n", ext_list[i].name, ext_list[i].cnt);
    }

    pclose(fp);

    return 0;
}