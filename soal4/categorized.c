#define _GNU_SOURCE
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <pthread.h>
#include <sys/wait.h>
#include <sys/stat.h>
#define MAX_SIZE 1024

typedef struct
{
    char name[MAX_SIZE];
    int cnt;
} Ext;

typedef struct
{
    char src[MAX_SIZE];
    char ext[10];
    int max;
} Arg;

FILE *open_log()
{
    FILE *log = fopen("log.txt", "a");
    time_t current_time = time(NULL);
    struct tm *local_time;

    current_time = time(NULL);
    local_time = localtime(&current_time);

    fprintf(log, "%02d-%02d-%04d %02d:%02d:%02d ",
            local_time->tm_mday, local_time->tm_mon + 1, local_time->tm_year + 1900,
            local_time->tm_hour, local_time->tm_min, local_time->tm_sec);

    return log;
}

void log_accessed(char *path)
{
    FILE *log = open_log();
    fprintf(log, "ACCESSED %s\n", path);
    fclose(log);
}

void log_made(char *path)
{
    FILE *log = open_log();
    fprintf(log, "MADE %s\n", path);
    fclose(log);
}

void log_moved(char *ext, char *src, char *dst)
{
    FILE *log = open_log();
    fprintf(log, "MOVED %s file : %s > %s\n", ext, src, dst);
    fclose(log);
}

char *lower_case(char *str)
{
    for (int i = 0; str[i]; i++)
        str[i] = tolower(str[i]);
    return str;
}

void sort_exts(Ext *exts, int size)
{
    Ext temp;
    for (int i = 0; i < size - 1; i++)
    {
        for (int j = i + 1; j < size; j++)
        {
            if (exts[i].cnt < exts[j].cnt)
                continue;
            temp = exts[i];
            exts[i] = exts[j];
            exts[j] = temp;
        }
    }
}

char *get_ext(char *filename)
{
    char dot = '.';
    char *ext = strrchr(filename, dot);
    if (ext == NULL)
        return "";
    // printf("%s", ext);
    return (ext + 1);
}

void clear_log()
{
    if (access("log.txt", F_OK) != 0)
        return;
    remove("log.txt");
}

void categorize(char *src, char *ext, int *cnt, int max)
{
    DIR *src_dir;
    struct dirent *src_ent;
    src_dir = opendir(src);

    if (src_dir == NULL)
    {
        printf("Failed to open directory %s\n", src);
        exit(EXIT_FAILURE);
    }
    else
        log_accessed(src);

    while ((src_ent = readdir(src_dir)) != NULL)
    {
        if (
            strcmp(src_ent->d_name, ".") == 0 ||
            strcmp(src_ent->d_name, "..") == 0)
            continue;

        if (src_ent->d_type == DT_DIR)
        {
            char new_src[MAX_SIZE];
            snprintf(new_src, MAX_SIZE, "%s/%s", src, src_ent->d_name);
            categorize(new_src, ext, cnt, max);
        }

        else if (src_ent->d_type == DT_REG)
        {
            char src_ext[10];
            strcpy(src_ext, get_ext(src_ent->d_name));
            if (strcmp(ext, "other") != 0 && strcmp(lower_case(src_ext), ext) != 0)
                continue;

            int dst_cnt = *cnt / 10 + 1;
            char dst[MAX_SIZE];

            if (dst_cnt > 1)
                snprintf(dst, MAX_SIZE, "categorized/%s (%d)", ext, dst_cnt);
            else
                snprintf(dst, MAX_SIZE, "categorized/%s", ext);
            if (access(dst, F_OK) != 0)
            {
                if (mkdir(dst, 0777) != 0)
                {
                    printf("Failed to make directory %s\n", dst);
                    exit(EXIT_FAILURE);
                }
                else
                    log_made(dst);
            }

            char filename[MAX_SIZE];
            snprintf(filename, MAX_SIZE, "%s/%s", src, src_ent->d_name);
            strcat(dst, "/");
            strcat(dst, src_ent->d_name);
            if (rename(filename, dst) != 0)
            {
                printf("Failed to move %s to %s\n", filename, dst);
                exit(EXIT_FAILURE);
            }
            else
                log_moved(ext, filename, dst);
            (*cnt)++;
        }
    }

    closedir(src_dir);
}

void *th_func(void *thread)
{
    Arg *arg = (Arg *)thread;
    int cnt = 0;
    categorize(arg->src, arg->ext, &cnt, arg->max);

    // printf("%d", cnt);
    Ext *result = (Ext *)malloc(sizeof(Ext));
    strcpy(result->name, arg->ext);
    result->cnt = cnt;
    // printf("%s", result->name);
    return result;
}

int extract_ext(Arg *thread, Ext *ext_list)
{

    pthread_t tid[MAX_SIZE];

    FILE *ext = fopen("extensions.txt", "r");

    char ext_type[10];
    int ext_size = 0;

    FILE *max = fopen("max.txt", "r");

    int max_size;
    fscanf(max, "%d", &max_size);

    while (!(fscanf(ext, "%s", ext_type) == EOF))
    {
        strcpy(thread[ext_size].src, "files");
        strcpy(thread[ext_size].ext, ext_type);
        thread[ext_size].max = max_size;
        pthread_create(&tid[ext_size], NULL, th_func, &thread[ext_size]);
        ext_size++;
    }

    for (int i = 0; i < ext_size; i++)
    {
        Ext *result;
        pthread_join(tid[i], (void **)&result);
        ext_list[i] = *result;
    }

    int cnt = 0;
    categorize("files", "other", &cnt, max_size);
    sprintf(ext_list[ext_size].name, "other");
    ext_list[ext_size].cnt = cnt;
    ext_size++;

    // for (int i = 0; i < ext_size; i++) {
    //     printf("%s : %d\n", ext_list[i].name, ext_list[i].cnt);
    // }

    return ext_size;
}

int main()
{
    char command[100];
    sprintf(command, "mkdir categorized");
    system(command);

    Ext ext[MAX_SIZE];
    Arg thread[MAX_SIZE];

    int size = extract_ext(thread, ext);

    sort_exts(ext, size);
    for (int i = 0; i < size; i++)
    {
        printf("%s : %d\n", ext[i].name, ext[i].cnt);
    }

    return 0;
}
