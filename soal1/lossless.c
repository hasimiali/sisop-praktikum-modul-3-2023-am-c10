#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <sys/shm.h>
#include <sys/ipc.h>

#define READ_END 0
#define WRITE_END 1

// ENCODE

// struktur yang digunakan untuk mendefinisikan node

typedef struct node_t
{
    struct node_t *left, *right;
    int freq;
    char c;
} *node;

// variabel global
int n_nodes = 0, qend = 1;        // variabel global untuk melacak no.of node dan akhir antrian
struct node_t pool[256] = {{0}};  // kumpulan node
node qqq[255], *q = qqq - 1;      // antrian prioritas
char *code[128] = {0}, buf[1024]; // array string dari kode untuk setiap huruf
int input_data = 0, output_data = 0;

// fungsi yang digunakan untuk membuat node baru
node new_node(int freq, char c, node a, node b)
{
    node n = pool + n_nodes++;
    if (freq != 0)
    {
        n->c = c;       // tetapkan karakter 'c' ke karakter node (akhirnya menjadi daun)
        n->freq = freq; // menetapkan frekuensi
    }
    else
    {
        n->left = a, n->right = b;   // jika tidak ada frekuensi yang disediakan dengan pemanggilan
        n->freq = a->freq + b->freq; // node yang dihapus di akhir que akan ditambahkan ke kiri dan kanan
    }
    return n;
}

// fungsi ued untuk menyisipkan node ke dalam antrian prioritas
void qinsert(node n)
{
    int j, i = qend++;
    while ((j = i / 2))
    {
        if (q[j]->freq <= n->freq)
            break;
        q[i] = q[j], i = j;
    }
    q[i] = n;
}

node qremove()
{
    int i, l;
    node n = q[i = 1];

    if (qend < 2)
        return 0;
    qend--;
    while ((l = i * 2) < qend)
    {
        if (l + 1 < qend && q[l + 1]->freq < q[l]->freq)
            l++;
        q[i] = q[l], i = l;
    }
    q[i] = q[qend];
    return n; // return the node
}

// ikuti builded huffman tree dan tetapkan kode untuk setiap karakter
void build_code(node n, char *s, int len)
{
    static char *out = buf;
    if (n->c)
    {
        s[len] = 0;            // jika node yang disediakan adalah leaf (end node)
        strcpy(out, s);        // itu berisi karakter
        code[(int)n->c] = out; // oleh karena itu kode disalin ke karakter yang relevan.
        out += len + 1;        // pointer keluar bertambah

        return;
    }

    s[len] = '0';
    build_code(n->left, s, len + 1); // perulangan digunakan untuk menulis kode
    s[len] = '1';
    build_code(n->right, s, len + 1); // jika benar tambahkan 1 dan jika benar tambahkan 0
}

void import_file(FILE *fp_in, unsigned int *freq, FILE *fout)
{
    char c, s[16] = {0}; // temporary variables
    int i = 0;
    printf("File Read:\n");
    // FILE *fout = fopen("temp.txt", "w");
    while ((c = fgetc(fp_in)) != EOF)
    {
        if (isalpha(c))
        {                   // hanya memproses karakter huruf
            c = tolower(c); // ubah ke lowercase
            freq[(int)c]++;
            putchar(c);
            fputc(c, fout);
        }
        // putchar(c);
    }
    for (i = 0; i < 128; i++)
        if (freq[i])
            qinsert(new_node(freq[i], i, 0, 0)); // masukkan node baru ke dalam antrian jika ada frekuensi
    while (qend > 2)
        qinsert(new_node(0, 0, qremove(), qremove())); // build the tree
    build_code(q[1], s, 0);                            // build the code for the characters
    // fclose(fout);
}

void encode(FILE *fp_in, unsigned int *freq, FILE *bin_file)
{

    char in, c, temp[20] = {0};
    int i, j = 0, k = 0, lim = 0;
    rewind(fp_in);
    for (i = 0; i < 128; i++)
    {
        if (freq[i])
            lim += (freq[i] * strlen(code[i]));
    }
    output_data = lim; // Data output sama dengan limit
    printf("\nEncoded:\n");
    for (i = 0; i < lim; i++)
    {
        if (temp[j] == '\0')
        {
            in = fgetc(fp_in);
            strcpy(temp, code[in]);
            printf("%s", code[in]);
            fprintf(bin_file, "%s", code[in]);
            j = 0;
        }
        if (temp[j] == '1')
            c = c | (1 << (7 - k)); // shifts 1 to relevant position and OR with the temporary char
        else if (temp[j] == '0')
            c = c | (0 << (7 - k)); // shifts 0 to relevant position and OR with the temporary char
        else
            printf("ERROR: Wrong input!\n");
        k++; // k digunakan untuk membagi string menjadi potongan 8 bit dan menyimpannya
        j++;
        if (((i + 1) % 8 == 0) || (i == lim - 1))
        {
            k = 0; // reset k
            c = 0; // reset character
        }
    }
    putchar('\n');
}

void print_code(unsigned int *freq, FILE *huffman_codes)
{
    int i;
    printf("\n---------CODE TABLE---------\n----------------------------\nCHAR  FREQ  CODE\n----------------------------\n");
    for (i = 0; i < 128; i++)
    {
        if (isprint((char)i) && code[i] != NULL && i != ' ')
        {
            printf("%-4c  %-4d  %16s\n", i, freq[i], code[i]);
            fprintf(huffman_codes, "%c %s\n", i, code[i]);
        }
        else if (code[i] != NULL)
        {
            switch (i)
            {
            case '\n':
                printf("\\n  ");
                break;
            case ' ':
                printf("\' \' ");
                break;
            case '\t':
                printf("\\t  ");
                break;
            default:
                printf("%0X  ", (char)i);
                break;
            }
            printf("  %-4d  %16s\n", freq[i], code[i]);
        }
    }
    printf("----------------------------\n");
}

// END

// DECODE

// struktur node pohon Huffman
struct Node
{
    char data;
    struct Node *left, *right;
};

// fungsi untuk membuat node pohon Huffman baru
struct Node *createNode(char data)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    newNode->data = data;
    newNode->left = newNode->right = NULL;
    return newNode;
}

// fungsi untuk melakukan dekompresi menggunakan kode Huffman
void decompress(struct Node *root, char *compressed_data, char *decompressed_data)
{
    struct Node *curr = root;
    int len = strlen(compressed_data);
    int j = 0;
    for (int i = 0; i < len; i++)
    {
        if (compressed_data[i] == '0')
        {
            curr = curr->left;
        }
        else
        {
            curr = curr->right;
        }
        if (curr->left == NULL && curr->right == NULL)
        {
            decompressed_data[j++] = curr->data;
            curr = root;
        }
    }
    decompressed_data[j] = '\0';
}

// END

int main(int argc, char *argv[])
{
    // We use two pipes
    // First pipe to send input string from parent
    // Second pipe to send concatenated string from child

    FILE *fp_in, *fp_out, *bin_file, *huffman_codes, *tmpfile; // FIFO pointers

    char file_name[50] = {0}; // file name
    unsigned int freq[128] = {0}, i;

    system("clear");
    printf("**********************************************************************\n\n");

    if (argc == 2)
    {
        strcpy(file_name, argv[1]); // commandline argument directly allows to compress the file
    }
    else if (argc > 2)
    {
        printf("Too many arguments supplied.\n");
    }
    else
    {
        printf("Please enter the file to be compressed\t: "); // else a prompt comes to enter the file name
        scanf("%s", file_name);
    }

    if (strlen(file_name) >= 50)
    {
        printf("ERROR: Enter a file name less than 50 chars");
        return 0;
    }

    // import the file into the program and update the huffman tree
    if ((fp_in = fopen(file_name, "r")) == NULL)
    { // open the file stream
        printf("\nERROR: No such file\n");
        return 0;
    }

    // pipe
    int fd[2];
    int x;
    int status = 0;

    int fd2[2];

    int fd3[2];

    int fd4[2];
    int fd5[2];

    int fd6[2];
    int fd7[2];
    int fd8[2];

    if (pipe(fd) == -1)
    {
        perror("Error saat membuat pipa");
        exit(1);
    }

    if (pipe(fd2) == -1)
    {
        perror("Error saat membuat pipa");
        exit(1);
    }

    if (pipe(fd3) == -1)
    {
        perror("Error saat membuat pipa");
        exit(1);
    }

    if (pipe(fd4) == -1)
    {
        perror("Error saat membuat pipa");
        exit(1);
    }

    if (pipe(fd5) == -1)
    {
        perror("Error saat membuat pipa");
        exit(1);
    }

    if (pipe(fd6) == -1)
    {
        perror("Error saat membuat pipa");
        exit(1);
    }

    if (pipe(fd7) == -1)
    {
        perror("Error saat membuat pipa");
        exit(1);
    }

    if (pipe(fd8) == -1)
    {
        perror("Error saat membuat pipa");
        exit(1);
    }

    pid_t p;
    p = fork();

    if (p < 0)
    {
        fprintf(stderr, "fork Failed");
        return 1;
    }

    // Parent process
    else if (p > 0)
    {
        char concat_str[100];
        tmpfile = fopen("temp.txt", "w");
        import_file(fp_in, freq, tmpfile);
        fclose(tmpfile);
        x = 5;

        // send frequency array to child
        write(fd2[WRITE_END], &freq, sizeof(unsigned int) * 128);
        write(fd[WRITE_END], &x, sizeof(int));
        write(fd3[WRITE_END], q, sizeof(node));
        write(fd4[1], code, sizeof(code));
        write(fd5[WRITE_END], buf, sizeof(buf));

        read(fd6[READ_END], &status, sizeof(int));

        if (status == 20)
        {
            FILE *fp;
            fp = fopen("huffman_codes.txt", "r");
            if (fp == NULL)
            {
                printf("File tidak dapat dibuka\n");
                return 1;
            }

            // membaca kode Huffman dari file dan membuat pohon Huffman
            struct Node *root = createNode('\0');
            char code2[100], c;
            while (fscanf(fp, "%c %s\n", &c, code2) != EOF)
            {
                struct Node *curr = root;
                int len = strlen(code2);
                for (int i = 0; i < len; i++)
                {
                    if (code2[i] == '0')
                    {
                        if (curr->left == NULL)
                        {
                            curr->left = createNode('\0');
                        }
                        curr = curr->left;
                    }
                    else
                    {
                        if (curr->right == NULL)
                        {
                            curr->right = createNode('\0');
                        }
                        curr = curr->right;
                    }
                }
                curr->data = c;
            }

            // menutup file huffman_codes.txt
            // fclose(fp);

            // // membuka file yang telah dikompresi
            fp = fopen("bin_file.bin", "rb");
            if (fp == NULL)
            {
                printf("File tidak dapat dibuka\n");
                return 1;
            }

            // // menghitung ukuran file dan membaca isi file
            fseek(fp, 0, SEEK_END);
            int file_size = ftell(fp);
            fseek(fp, 0, SEEK_SET);
            char *compressed_data = (char *)malloc(file_size + 1);
            fread(compressed_data, 1, file_size, fp);
            compressed_data[file_size] = '\0';

            // menutup file yang telah dikompresi
            fclose(fp);

            // melakukan dekompresi menggunakan kode Huffman
            char *decompressed_data = (char *)malloc(file_size + 1);
            decompress(root, compressed_data, decompressed_data);
            printf("\nHasil dekompresi: %s\n", decompressed_data);

            // menulis hasil dekompresi ke file output
            fp = fopen("decompressed_data.txt", "w");
            if (fp == NULL)
            {
                printf("File tidak dapat dibuka\n");
                return 1;
            }
            fwrite(decompressed_data, 1, strlen(decompressed_data), fp);
            fclose(fp);

            // membebaskan memori yang digunakan
            // free(binaryString);
            read(fd7[READ_END], &input_data, sizeof(int));
            read(fd8[READ_END], &output_data, sizeof(int));
            printf("\nInput bytes:\t\t%d\n", input_data);
            output_data = (output_data % 8) ? (output_data / 8) + 1 : (output_data / 8);
            printf("Output bytes:\t\t%d\n", output_data);

            printf("\nCompression ratio:\t%.2f%%\n\n\n", ((double)(input_data - output_data) / input_data) * 100);

            free(decompressed_data);
        }

        wait(NULL);
    }

    // child process
    else
    {
        read(fd[READ_END], &x, sizeof(int));
        read(fd2[READ_END], &freq, sizeof(unsigned int) * 128);
        read(fd3[READ_END], q, sizeof(node));
        read(fd4[0], code, sizeof(code));
        read(fd5[READ_END], buf, sizeof(buf));
        // melakukan kompresi file setelah menerima hasil frequensi dari parent process
        if (x == 5)
        {
            huffman_codes = fopen("huffman_codes.txt", "w");
            print_code(freq, huffman_codes); // print the code table

            // Encode and save the encoded file
            bin_file = fopen("bin_file.bin", "w");
            tmpfile = fopen("temp.txt", "r");
            encode(tmpfile, freq, bin_file);

            fclose(tmpfile);
            fclose(fp_in);
            fclose(bin_file);

            for (i = 0; i < 128; i++)
                input_data += freq[i]; // calculate input bytes

            status = 20;
            write(fd7[WRITE_END], &input_data, sizeof(int));
            write(fd8[WRITE_END], &output_data, sizeof(int));
            write(fd6[WRITE_END], &status, sizeof(int));
        }
        exit(0);
    }
}