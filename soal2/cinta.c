#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

int row = 4, col = 5;
long long number;
int arrKelola[20];
long long arrRes[20];

typedef struct args_s
{
	int index;
	int num_fact;
} s_args;
s_args str_fac[20];
void *faktorial(void *args)
{

	s_args *data;
	data = (s_args *)args;

	number = data->num_fact;
	// printf("For num %lld start\n", number);
	for (int i = number - 1; i > 0; i--)
		number *= i;
	// printf("%lld ", number);
	arrRes[data->index] = number;
	// printf("For num %lld finish\n", number);
	return NULL;
}

void main()
{
	clock_t start_time = clock();
	key_t key = 1234;
	int *matriks;

	int shmid = shmget(key, sizeof(int) * row * col, IPC_CREAT | 0666);
	matriks = (int *)shmat(shmid, NULL, 0);

	printf("Matriks diakses dari matriks cinta.c\n");

	printf("[[");
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			if (j != col - 1)
				printf("%d,", matriks[i * col + j]);
			else
				printf("%d", matriks[i * col + j]);
			arrKelola[i * col + j] = matriks[i * col + j];
		}
		if (i != row - 1)
			printf("], [");
		else
			printf("]]\n");
	}

	int totalElement = 20;
	pthread_t t_id[20];
	for (int i = 0; i < totalElement; i++)
	{

		str_fac[i].index = i;
		str_fac[i].num_fact = arrKelola[i];

		pthread_create(&t_id[i], NULL, &faktorial, (void *)&str_fac[i]);
	}

	for (int i = 0; i < totalElement; i++)
	{
		pthread_join(t_id[i], NULL);
	}

	for (int i = 0; i < totalElement; i++)
		printf("%lld ", arrRes[i]);
	printf("\ncinta.c berakhir di sini\n");
	shmdt(matriks);
	shmctl(shmid, IPC_RMID, NULL);
	clock_t end_time = clock();
	printf("Time %lf", (double)(end_time - start_time) / CLOCKS_PER_SEC);
}
