#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <pthread.h>

void faktorial(int args)
{
	long long number = args;
	for (int i = number - 1; i > 0; i--)
		number *= i;
	printf("%lld\t", number);
}

void main()
{
	clock_t start_time = clock();
	int row = 4, col = 5;
	int totalElement = row * col;
	key_t key = 1234;
	int *matriks;
	int arrKelola[totalElement];

	int shmid = shmget(key, sizeof(int) * row * col, IPC_CREAT | 0666);
	matriks = (int *)shmat(shmid, NULL, 0);

	printf("Matriks diakses dari matriks cinta.c\n");

	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			printf("%d\t", matriks[i * col + j]);
			arrKelola[i * col + j] = matriks[i * col + j];
		}
		printf("\n");
	}

	for (int i = 0; i < totalElement; i++)
	{
		faktorial(arrKelola[i]);
	}

	printf("\ncinta.c berakhir di sini\n");
	shmdt(matriks);
	shmctl(shmid, IPC_RMID, NULL);
	clock_t end_time = clock();
	printf("Time %lf", (double)(end_time - start_time) / CLOCKS_PER_SEC);
}
