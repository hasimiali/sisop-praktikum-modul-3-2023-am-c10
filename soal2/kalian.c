#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

int row = 4, col = 5;
int rowA = 4, colA = 2, rowB = 2, colB = 5;
int matA[4][2];
int matB[2][5];
int hasilMult[4][5];
int *matriks;

void matMult()
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			for (int k = 0; k < 2; k++)
			{
				hasilMult[i][j] += matA[i][k] * matB[k][j];
			}
		}
	}
}

void main()
{
	key_t key = 1234;

	int shmid = shmget(key, sizeof(int) * row * col, IPC_CREAT | 0666);
	matriks = (int *)shmat(shmid, NULL, 0);

	matA[0][0] = 1;
	matA[0][1] = 2;
	matA[1][0] = 2;
	matA[1][1] = 1;
	matA[2][0] = 1;
	matA[2][1] = 2;
	matA[3][0] = 2;
	matA[3][1] = 1;

	matB[0][0] = 2;
	matB[0][1] = 1;
	matB[0][2] = 1;
	matB[0][3] = 2;
	matB[0][4] = 2;
	matB[1][0] = 1;
	matB[1][1] = 1;
	matB[1][2] = 2;
	matB[1][3] = 2;
	matB[1][4] = 1;

	int matAUpperBound = 5, matBUpperBound = 4;
	for (int i = 0; i < rowA; i++)
	{
		for (int j = 0; j < colA; j++)
		{
			matA[i][j] = (rand() % (matAUpperBound)) + 1;
		}
	}

	for (int i = 0; i < rowB; i++)
	{
		for (int j = 0; j < colB; j++)
		{
			matB[i][j] = (rand() % (matBUpperBound)) + 1;
		}
	}

	printf("Proses perkalian matriks A dan B\n");

	matMult();
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			matriks[i * col + j] = hasilMult[i][j];
			printf("%d\t", matriks[i * col + j]);
		}
		printf("\n");
	}

	sleep(5);
	printf("kalian.c berakhir di sini\n");
	shmdt(matriks);
	shmctl(shmid, IPC_RMID, NULL);
}
